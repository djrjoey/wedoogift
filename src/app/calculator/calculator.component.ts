import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Combination } from '../models/combination';
import { CalculatorService } from '../services/calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent implements OnInit {

  /**
   * Form
   */
  calculatorForm: FormGroup = new FormGroup({});

  /**
   * Combination of cards
   */
  combination: Combination = {};

  /**
   * The desired amount combination is possible
   */
  desiredAmountPossible = false;

  /**
   * Amount emitted
   */
  @Output()
  amount = new EventEmitter<number>();

  constructor (
    private formBuilder: FormBuilder,
    private calculatorService: CalculatorService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  /**
   * Initialize the form
   */
  initForm() {
    this.calculatorForm = this.formBuilder.group({
      amount: [0, [Validators.required, Validators.min(0)]]
    });
  }

  /**
   * Fill the previous amount on button click
   */
  onPrevAmount() {
    this.calculatorForm.controls.amount.setValue(this.combination.floor?.value);
    this.searchCombination();
  }

  /**
   * Fill the next amount on button click
   */
  onNextAmount() {
    this.calculatorForm.controls.amount.setValue(this.combination.ceil?.value);
    this.searchCombination();
  }

  /**
   * Search the combination of the desired amount
   */
  searchCombination() {
    this.calculatorService.searchCombination(this.calculatorForm.controls.amount.value).subscribe(
      data => {
        console.log(data);
        this.combination = data;
        this.desiredAmountPossible = (this.combination.equal !== undefined);
      }
    );
  }

  /**
   * Submit the form
   */
  onSubmit() {
    this.searchCombination();
    this.amount.emit(this.calculatorForm.controls.amount.value);
  }

}
