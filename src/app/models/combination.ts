export interface Composition {
    value: number,
    cards: number[],
}

export interface Combination {
    equal?: Composition,
    floor?: Composition,
    ceil?: Composition,
}