import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Combination } from '../models/combination';
import { environment } from 'src/environments/environment';

export const API_URL = `${environment.apiEndpoint}/shop/${environment.shopId}`;

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  constructor (
    private http: HttpClient
  ) { }

  /**
   *  Call the API search-combination
   * @param amount Desired amount
   * @returns Lower, higher and/or equal combinations of cards of the desired amount
   */
  searchCombination(amount: number): Observable<Combination> {
    const headers = new HttpHeaders().append('Authorization', environment.token);
    const params = new HttpParams().set('amount', amount);
    return this.http.get<Combination>(
      `${API_URL}/search-combination`,
      {
        headers: headers,
        params: params
      }
    );
  }
}
